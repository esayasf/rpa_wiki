from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import time
import json
import os

if os.access('/app/docs/2852.xlsx', os.F_OK):
    os.remove('/app/docs/2852.xlsx')

#### Función para poder descargar ficheros desde docker en modo descabezado

def enable_download_headless(browser, download_dir):
    browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
    params = {'cmd':'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': download_dir}}
    browser.execute("send_command", params)

##### Colocar en primera posición --no-sandbox

if __name__ == '__main__':
    options = Options()
    options.add_argument('--no-sandbox')    ##############
    options.add_argument("--disable-notifications")
    options.add_argument('--verbose')
    options.add_experimental_option("prefs", {
        "download.default_directory": '/app/docs',
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing_for_trusted_sources_enabled": False,
        "safebrowsing.enabled": False
    })
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-software-rasterizer')
    options.add_argument('--headless')
    # driver_path = f'{os.getcwd()}\\webdriver\\chromedriver.exe'

##### Al haber añadido en el PATH la ruta de webdriver mediante nuestro Dockerfile
    # no es necesario añadir drive_path en webdriver.Chrome(driver_path, options)

    driver = webdriver.Chrome(chrome_options=options)

##### ##### Llamamos a la función cambiando ‘/app/docs’ por la ruta donde descargara los ficheros

    enable_download_headless(driver, '/app/docs')

    driver.get('https://www.ine.es/')

#inicializamos el navegador:

#driver.get('https://www.ine.es/')

WebDriverWait(driver, 5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/section[3]/div/ul/li[3]/div/a/p[1]/img')))\
    .click()

WebDriverWait(driver, 5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/div[2]/div[2]/ul/li[1]/span')))\
    .click()

WebDriverWait(driver, 5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/div[2]/div[2]/ul/li[1]/div/ul/li/table/tbody/tr[1]/th/a')))\
    .click()

WebDriverWait(driver,5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/section[2]/div/div/div[1]/ul/li/ul/li[1]/a')))\
    .click()

WebDriverWait(driver, 5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/div[4]/div[2]/ol/li[1]/ol/li[1]/a[3]')))\
    .click()

WebDriverWait(driver, 10)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/form/ul/li[1]/ul/li[1]/div/fieldset/div[2]/button[2]')))\
    .click()

WebDriverWait(driver, 10)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/form/ul/li[1]/ul/li[2]/div/fieldset/div[2]/button[2]')))\
    .click()

WebDriverWait(driver, 10)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/form/ul/li[1]/ul/li[3]/div/fieldset/div[3]/button[2]')))\
    .click()

# Recogemos los datos desde el json creado desde nuestro html

with open('file.json') as file:
    datos = json.load(file)
    dato_provincia = datos['provincia'].split('\r')[0]
    dato_anio = datos['anio']
    dato_genero = datos['genero'].capitalize()

elemento = driver.find_element_by_id('cri28369')            #añadimos a la variable todos los elementos de la id "####"
for option in elemento.find_elements_by_tag_name('option'): #iteramos todos los elementos
    if option.text == dato_provincia:                       #Comparamos el text del elemento con nuestra variable de provincia
        option.click()                                      #Si coincide la seleccionamos y salimos del bucle
        break

elemento = driver.find_element_by_id('cri28370')
for option in elemento.find_elements_by_tag_name('option'):

    if option.text == dato_genero:
        option.click()
        break

elemento = driver.find_element_by_id('periodo')
for option in elemento.find_elements_by_tag_name('option'):

    if option.text == dato_anio:
        option.click()
        break


WebDriverWait(driver, 10)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/form/div[5]/input[3]')))\
    .click()

WebDriverWait(driver, 10)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/ul[1]/li/div/div/form[2]/button')))\
    .click()

waitc = WebDriverWait(driver, 10)

###### Seleccionamos el IFRAME de nombre 'thickBoxINEfrm'
waitc.until(ec.frame_to_be_available_and_switch_to_it('thickBoxINEfrm'))
WebDriverWait(driver, 10)

i = driver.find_element_by_xpath('/html/body/form/ul/li[3]/label/input')

driver.execute_script('arguments[0].click();', i)
driver.switch_to.default_content()

time.sleep(5)

driver.close()








