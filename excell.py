import openpyxl


wb = openpyxl.load_workbook('docs/2852.xlsx')
ws = wb.active
print(ws)
genero = ws['B7'].value
provincia = ws['A9'].value.split()[1]
anio = ws['B8'].value
demografia = int(ws['B9'].value)

print(f'Provincia: {provincia}\nAño: {anio}\nGenero: {genero}\nDemografía: {demografia}')