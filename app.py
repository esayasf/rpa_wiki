import time

from flask import Flask, request
from flask import render_template
import json
import os
import openpyxl
#from excell import Datos as extraer



app = Flask(__name__) #nuevo objeto

@app.route('/', methods = ['GET', 'POST'])          # decorador de ruta
def index():
    lista = []
    datos = []
    if os.access('infoselect.txt', os.F_OK):        # Si el fichero existe devuelve True

        with open('infoselect.txt', 'r', encoding='UTF-8') as data:   # Abre el fichero infoselect
            for linea in data:                      # Por cada linea
                lista.append(linea)                 # Añade a la lista

        if request.method == 'POST':                # Si la respuesta es POST (le damos a enviar en nuestro html):


            with open('file.json', 'w') as file:    # Crea el fichero file.json
                json.dump(request.form, file)       # Recoge toda la informacion introducida en la página HTML en file.json

            os.system('python selen.py')            # Ejecuta el comando "python selen.py"
            time.sleep(10)
            wb = openpyxl.load_workbook('docs/2852.xlsx')   #cargamos el archivo xlsx y lo manipulamos para sacar la información ed las celdas
            ws = wb.active
            genero = ws['B7'].value
            provincia = ws['A9'].value.split()[1]
            anio = ws['B8'].value
            demografia = ws['B9'].value
            datos = [anio, genero, provincia, demografia]
           # return render_template('index.html', lista=lista[1:], datos=datos)  # Renderiza la plantilla index.html y envia la variable lista

            return render_template('index.html', lista=lista[1:], datos=datos) # Renderiza la plantilla index.html y envia la variable lista
        return render_template('index.html', lista=lista[1:], datos=datos)
    else:

        os.system('python informacion.py')
        time.sleep(5)
        return index()

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)

