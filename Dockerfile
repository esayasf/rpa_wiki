# syntax=docker/dockerfile:1
FROM python:3.8
COPY . /app
WORKDIR /app
RUN mkdir __logger

# LINEAS PARA LA INSTALACIÓN DE GOOGLE DRIVE
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable

# LINEAS PARA LA INSTALACIÓN DE CHROMEDRIVER
RUN apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/
# set display port to avoid crash
ENV DISPLAY=:99

# ENTRADA PARA AÑADIR A NUESTRO PATH LA RUTA A NUESTRO CHROMEDRIVER.EXE
#/app/webdriver se cambiara por la ruta del directorio donde almacenemos nuestro chromedriver.exe
ENV PATH=/app/webdriver:$PATH

RUN pip install --upgrade pip
RUN pip install -r requirements.txt
CMD ["python", "./app.py"]



